package com.example.demo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.sql.DriverManager
import java.sql.SQLException

@SpringBootApplication
class DemoApplication

fun main() {
	// This should not be available
	println("Trying to open h2 connection, this should fail...")
	try {
		DriverManager.getConnection("jdbc:h2:mem:")
		println("It worked, h2 was *not* successfully excluded")
	} catch (e: SQLException) {
		e.printStackTrace()
		println("It failed, h2 was successfully excluded")
	}
	println("Trying to open sqlite connection, this should only work in docker...")
	try {
		DriverManager.getConnection("jdbc:sqlite::memory:")
		println("It worked, sqlite was successfully included")
	} catch (e: SQLException) {
		e.printStackTrace()
		println("It failed, sqlite was *not* successfully included")
	}

	//runApplication<DemoApplication>(*args)
}
